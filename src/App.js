import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  // hier kun je functies plaatsen

  state = {
    getalEen: 10,
    getalTwee: 10,
    uitkomst: 0,
  } 

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  printMijnNaam = (mijnNaam) => {
    console.log(mijnNaam)
  }

  simpeleBerekening = (getalEen, getalTwee) => {
    const uitkomstUitBerekening = getalEen * getalTwee
    this.setState({
      uitkomst: uitkomstUitBerekening
    })
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <button onClick={() => this.printMijnNaam('irem')}>Print mijn naam</button>
        
        <input type="number" value={this.state.getalEen}  onChange={this.handleChange('getalEen')}/>
        <input type="number" value={this.state.getalTwee} onChange={this.handleChange('getalTwee')}/>
        <button onClick={() => this.simpeleBerekening(this.state.getalEen, this.state.getalTwee)}>berekening uitvoeren</button>
      Uitkomst {this.state.uitkomst}
      </div>
    );
  }
}

export default App;
